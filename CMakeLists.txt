cmake_minimum_required(VERSION 3.20)

set(CMAKE_CUDA_HOST_COMPILER g++)

#SET(CMAKE_CUDA_COMPILER /usr/local/cuda/bin/nvcc)

project(clue LANGUAGES CXX CUDA)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

set(CMAKE_CUDA_STANDARD 14)
set(CMAKE_CUDA_STANDARD_REQUIRED TRUE)

set(rmm_ROOT /install/path)

find_package(CUDAToolkit REQUIRED)
find_package(rmm)

add_executable(clue
    src/CLUEAlgo.cc
    src/CLUEAlgoGPU.cu
    src/GPUVecArray.cu
    src/main.cc
    include/CLUEAlgo.h
    include/GPUCommon.h
    include/GPUVecArray.cuh
    include/CLUEAlgoGPU.cuh
    include/LayerTiles.h
    include/LayerTilesConstants.h
    include/LayerTilesGPU.cuh
    include/Points.h
)

#set_source_files_properties(src/main.cc PROPERTIES LANGUAGE CUDA)

#target_include_directories(clue PRIVATE ${CMAKE_CUDA_TOOLKIT_INCLUDE_DIRECTORIES} include cupla/include cupla/alpaka/include /install/path/include)

target_include_directories(clue PRIVATE ${CMAKE_CUDA_TOOLKIT_INCLUDE_DIRECTORIES} include)

target_link_libraries(clue PRIVATE rmm::rmm)

target_link_libraries(clue PRIVATE CUDA::cudart)

if (CMAKE_BUILD_TYPE STREQUAL "Debug")
  target_compile_options(clue PRIVATE $<$<COMPILE_LANGUAGE:CUDA>:
    -expt-relaxed-constexpr
    -w
    -Xcompiler
    -g -G
  >)
else()
  target_compile_options(clue PRIVATE $<$<COMPILE_LANGUAGE:CUDA>:
    -expt-relaxed-constexpr
    -w
    -Xcompiler
  >)
endif()

#add_compile_options(-DUSE_CUPLA)

set_property(TARGET clue PROPERTY CUDA_SEPARABLE_COMPILATION ON)
set_target_properties(clue PROPERTIES CUDA_ARCHITECTURES "60;75;80")

