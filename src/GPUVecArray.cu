#include "GPUVecArray.cuh"

using namespace GPU;

// thread-safe version of the vector, when used in a CUDA kernel
template<class T>
__device__
int Vector<T>::push_back(const T &element) {
    auto previousSize = atomicAdd(&m_size, 1);
    if (previousSize < m_capacity) {
        m_data[previousSize] = element;
        return previousSize;
    } else {
        atomicSub(&m_size, 1);
        return -1;
    }
}

template<class T>
template<class... Ts>
__device__
int Vector<T>::emplace_back(Ts &&... args) {
    auto previousSize = atomicAdd(&m_size, 1);
    if (previousSize < m_capacity) {
        (new(&m_data[previousSize]) T(std::forward<Ts>(args)...));
        return previousSize;
    } else {
        atomicSub(&m_size, 1);
        return -1;
    }
}

template int GPU::Vector<int>::push_back(const int &element);
