#include <math.h>
#include <limits>
#include <iostream>

#include <cuda_runtime.h>

#include "CLUEAlgoGPU.cuh"

static const int THREADBLOCK_SIZE = 512;

__device__ void PointsPtr::setClusterIndex(int i, int value) {
    atomicExch(&clusterIndex[i], value);
}

__global__ void kernel_compute_histogram(GPU::LayerTilesArray *d_hist,
                                         const PointsPtr d_points,
                                         int numberOfPoints) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < numberOfPoints) {
        GPU::LayerTiles* layerTiles = d_hist->get(d_points.layer[i]);

        // push index of points into tiles
        layerTiles->fill(d_points.x[i], d_points.y[i], i);
    }
}

__global__ void kernel_calculate_density(const GPU::LayerTilesArray *d_hist,
                                         PointsPtr d_points,
                                         float dc,
                                         int numberOfPoints) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < numberOfPoints) {
        float rhoi = 0.f;
        int layeri = d_points.layer[i];
        float xi = d_points.x[i];
        float yi = d_points.y[i];

        const GPU::LayerTiles* layerTiles = d_hist->get(layeri);

        // get search box
        int4 search_box = layerTiles->searchBox(xi - dc, xi + dc, yi - dc, yi + dc);

        // loop over bins in the search box
        for (int xBin = search_box.x; xBin < search_box.y + 1; ++xBin) {
            for (int yBin = search_box.z; yBin < search_box.w + 1; ++yBin) {
                // get the id of this bin
                int binId = layerTiles->getGlobalBinByBin(xBin, yBin);

                const GPU::Vector<int>& bin = *layerTiles->get(binId);
                // get the size of this bin
                int binSize = bin.size();

                // interate inside this bin
                for (int binIter = 0; binIter < binSize; binIter++) {
                    int j = bin[binIter];
                    // query N_{dc_}(i)
                    float xj = d_points.x[j];
                    float yj = d_points.y[j];
                    float dist_ij = std::sqrt((xi - xj) * (xi - xj) + (yi - yj) * (yi - yj));
                    if (dist_ij <= dc) {
                        // sum weights within N_{dc_}(i)
                        rhoi += (i == j ? 1.f : 0.5f) * d_points.weight[j];
                    }
                } // end of interate inside this bin
            }
        } // end of loop over bins in search box
        d_points.rho[i] = rhoi;
    }
} //kernel

__global__ void kernel_calculate_distanceToHigher(const GPU::LayerTilesArray* d_hist,
                                                  PointsPtr d_points,
                                                  float outlierDeltaFactor,
                                                  float dc,
                                                  int numberOfPoints) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    const float dm = outlierDeltaFactor * dc;

    if (i < numberOfPoints) {
        const int layeri = d_points.layer[i];

        float deltai = std::numeric_limits<float>::max();
        int nearestHigheri = -1;
        float xi = d_points.x[i];
        float yi = d_points.y[i];
        float rhoi = d_points.rho[i];

        const GPU::LayerTiles* layerTiles = d_hist->get(layeri);

        // get search box
        int4 search_box = layerTiles->searchBox(xi - dm, xi + dm, yi - dm, yi + dm);

        // loop over all bins in the search box
        for (int xBin = search_box.x; xBin < search_box.y + 1; ++xBin) {
            for (int yBin = search_box.z; yBin < search_box.w + 1; ++yBin) {
                // get the id of this bin
                const int binId = layerTiles->getGlobalBinByBin(xBin, yBin);

                const GPU::Vector<int>* layerTile = layerTiles->get(binId);

                // get the size of this bin
                int binSize = layerTile->size();

                // iterate inside this bin
                for (int binIter = 0; binIter < binSize; binIter++) {
                    int j = (*layerTile)[binIter];
                    // query N'_{dm}(i)
                    float xj = d_points.x[j];
                    float yj = d_points.y[j];
                    float dist_ij = sqrt((xi - xj) * (xi - xj) + (yi - yj) * (yi - yj));

                    float rhoj = d_points.rho[j];

                    bool foundHigher = (rhoj > rhoi) || ((rhoj == rhoi) && (j > i));
                    // in the rare case where rho is the same, use detid

                    if (foundHigher && dist_ij <= dm && dist_ij < deltai) { // definition of N'_{dm}(i)
                        // find the nearest point within N'_{dm}(i)
                        // update deltai and nearestHigheri
                        deltai = dist_ij;
                        nearestHigheri = j;
                    }
                } // end of interate inside this bin
            }
        } // end of loop over bins in search box

        d_points.delta[i] = deltai;
        d_points.nearestHigher[i] = nearestHigheri;
    }
} //kernel

__global__ void kernel_find_clusters( GPU::Vector<int>* d_seeds,
                                      GPU::Matrix* d_followers,
                                      PointsPtr d_points,
                                      float outlierDeltaFactor, float dc, float rhoc,
                                      int numberOfPoints
                                      ) 
{
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  
  if (i < numberOfPoints) {
    // initialize clusterIndex
    d_points.clusterIndex[i] = -1;
    // determine seed or outlier
    float deltai = d_points.delta[i];
    float rhoi = d_points.rho[i];
    bool isSeed = (deltai > dc) && (rhoi >= rhoc);
    bool isOutlier = (deltai > outlierDeltaFactor * dc) && (rhoi < rhoc);

      if (isSeed) {
          // set isSeed as 1
          d_points.isSeed[i] = 1;
          d_seeds->push_back(i); // head of d_seeds
      } else if (!isOutlier) {
          // register as follower at its nearest higher
          GPU::Vector<int>* followers = d_followers->get(d_points.nearestHigher[i]);
          followers->push_back(i);
      }
  }
} //kernel

__global__ void kernel_assign_clusters(const GPU::Vector<int> *seeds,
                                       const GPU::Matrix *d_followers,
                                       PointsPtr d_points) {

    int idxCls = blockIdx.x * blockDim.x + threadIdx.x;
    const auto nSeeds = seeds->size();

    if (idxCls < nSeeds) {
        int localStack[LOCAL_STACK_SIZE_PER_SEED] = {-1};
        int localStackSize = 0;

        // assign cluster to seed[idxCls]
        int idxThisSeed = (*seeds)[idxCls];
        d_points.clusterIndex[idxThisSeed] = idxCls;

        // push_back idThisSeed to localStack
        localStack[localStackSize] = idxThisSeed;
        localStackSize++;

        // process all elements in localStack
        while (localStackSize > 0) {
            // get last element of localStack
            const int idxEndOflocalStack = localStack[localStackSize - 1];
            const int temp_clusterIndex = d_points.clusterIndex[idxEndOflocalStack];

            // pop_back last element of localStack
            localStack[localStackSize - 1] = -1;
            localStackSize--;

            // loop over followers of last element of localStack
            const GPU::Vector<int>& followers = *d_followers->get(idxEndOflocalStack);
            for (int j : followers) {
                // pass id to follower
                d_points.clusterIndex[j] = temp_clusterIndex;

                // push_back follower to localStack
                localStack[localStackSize] = j;
                localStackSize++;
            }
        }
    }
} //kernel

// private methods
void CLUEAlgoGPU::init_device(int pointCount, rmm::cuda_stream_view &stream_view) {
    // input variables
    CUDA_CALL(cudaMalloc(&d_points.x, sizeof(float) * pointCount));
    CUDA_CALL(cudaMalloc(&d_points.y, sizeof(float) * pointCount));
    CUDA_CALL(cudaMalloc(&d_points.layer, sizeof(int) * pointCount));
    CUDA_CALL(cudaMalloc(&d_points.weight, sizeof(float) * pointCount));
    // result variables
    CUDA_CALL(cudaMalloc(&d_points.rho, sizeof(float) * pointCount));
    CUDA_CALL(cudaMalloc(&d_points.delta, sizeof(float) * pointCount));
    CUDA_CALL(cudaMalloc(&d_points.nearestHigher, sizeof(int) * pointCount));
    CUDA_CALL(cudaMalloc(&d_points.clusterIndex, sizeof(int) * pointCount));
    CUDA_CALL(cudaMalloc(&d_points.isSeed, sizeof(int) * pointCount));

    // algorithm internal variables
    d_seeds = new GPU::Vector<int>(_seedsData);
    d_followers = new GPU::Matrix(_followersData, pointCount, MAX_N_FOLLOWERS);
    d_hist = new GPU::LayerTilesArray(_layerTilesData, NLAYERS, LayerTilesConstants::gridSize2D,
                                    LayerTilesConstants::maxTileDepth);

    std::cout << "Allocated data size - seeds: " << _seedsData.size() << " followers: " << _followersData.size()
              << " tiles:" << _layerTilesData.size() << std::endl;
}

void CLUEAlgoGPU::free_device(){
  // input variables
  CUDA_CALL(cudaFree(d_points.x));
  CUDA_CALL(cudaFree(d_points.y));
  CUDA_CALL(cudaFree(d_points.layer));
  CUDA_CALL(cudaFree(d_points.weight));
  // result variables
  CUDA_CALL(cudaFree(d_points.rho));
  CUDA_CALL(cudaFree(d_points.delta));
  CUDA_CALL(cudaFree(d_points.nearestHigher));
  CUDA_CALL(cudaFree(d_points.clusterIndex));
  CUDA_CALL(cudaFree(d_points.isSeed));

  // algorithm internal variables
  //cudaFree(d_hist);
  //cudaFree(d_seeds);
  //cudaFree(d_followers);
  delete d_seeds;
  delete d_followers;
  delete d_hist;
}

void CLUEAlgoGPU::copy_todevice() {
    std::cout << "Copy to device" << std::endl;

    // input variables
    CUDA_CALL(cudaMemcpy(d_points.x, points_.x.data(), sizeof(float) * points_.n, cudaMemcpyHostToDevice));
    CUDA_CALL(cudaMemcpy(d_points.y, points_.y.data(), sizeof(float) * points_.n, cudaMemcpyHostToDevice));
    CUDA_CALL(cudaMemcpy(d_points.layer, points_.layer.data(), sizeof(int) * points_.n, cudaMemcpyHostToDevice));
    CUDA_CALL(cudaMemcpy(d_points.weight, points_.weight.data(), sizeof(float) * points_.n, cudaMemcpyHostToDevice));
}

void CLUEAlgoGPU::clear_set() {
    std::cout << "Clearing data sets" << std::endl;

    // result variables
    CUDA_CALL(cudaMemset(d_points.rho, 0x00, sizeof(float) * points_.n));
    CUDA_CALL(cudaMemset(d_points.delta, 0x00, sizeof(float) * points_.n));
    CUDA_CALL(cudaMemset(d_points.nearestHigher, 0x00, sizeof(int) * points_.n));
    CUDA_CALL(cudaMemset(d_points.clusterIndex, 0x00, sizeof(int) * points_.n));
    CUDA_CALL(cudaMemset(d_points.isSeed, 0x00, sizeof(int) * points_.n));

    // algorithm internal variables
    d_hist->reset();
    d_seeds->reset();
    d_followers->reset();
}

void CLUEAlgoGPU::copy_tohost(){
  // result variables
  CUDA_CALL(cudaMemcpy(points_.clusterIndex.data(), d_points.clusterIndex, sizeof(int)*points_.n, cudaMemcpyDeviceToHost));
  if (verbose_) {
    // other variables, copy only when verbose_==True
    CUDA_CALL(cudaMemcpy(points_.rho.data(), d_points.rho, sizeof(float) * points_.n, cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(points_.delta.data(), d_points.delta, sizeof(float) * points_.n, cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(points_.nearestHigher.data(), d_points.nearestHigher, sizeof(int) * points_.n, cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(points_.isSeed.data(), d_points.isSeed, sizeof(int) * points_.n, cudaMemcpyDeviceToHost));
  }
}

void measureElapsedTime(const std::string& title, cudaEvent_t& start, cudaEvent_t& stop) {
    CUDA_CALL(cudaEventRecord(stop));
    CUDA_CALL(cudaEventSynchronize(stop));
    float milliseconds;
    CUDA_CALL(cudaEventElapsedTime(&milliseconds, start, stop));
    std::cout << title << milliseconds << " ms" << std::endl;
}

void CLUEAlgoGPU::makeClusters() {
    copy_todevice();
    clear_set();

    std::cout << "Make clusters" << std::endl;

    ////////////////////////////////////////////
    // calculate rho, delta and find seeds
    // 1 point per thread
    ////////////////////////////////////////////
    cudaEvent_t start, stop;
    CUDA_CALL(cudaEventCreate(&start));
    CUDA_CALL(cudaEventCreate(&stop));

    CUDA_CALL(cudaEventRecord(start));
    kernel_compute_histogram<<<ceil(points_.n / 256.0), 256>>>(d_hist, d_points, points_.n);
    measureElapsedTime("--- computeHistogram:     ", start, stop);

    const dim3 blockSize(THREADBLOCK_SIZE);
    const dim3 gridSize(ceil(points_.n / static_cast<float>(blockSize.x)));

    CUDA_CALL(cudaEventRecord(start));
    kernel_calculate_density<<<gridSize, blockSize>>>(d_hist, d_points, dc_, points_.n);
    measureElapsedTime("--- calculateLocalDensity:     ", start, stop);

    kernel_calculate_distanceToHigher<<<gridSize, blockSize>>>(d_hist, d_points,outlierDeltaFactor_, dc_, points_.n);
    measureElapsedTime("--- calculateDistanceToHigher:     ", start, stop);

    kernel_find_clusters<<<gridSize, blockSize>>>(d_seeds, d_followers, d_points, outlierDeltaFactor_, dc_, rhoc_,
                                                  points_.n);
    measureElapsedTime("--- findSeedAndFollowers:     ", start, stop);
    CUDA_CALL(cudaDeviceSynchronize());

    ////////////////////////////////////////////
    // assign clusters
    // 1 point per seeds
    ////////////////////////////////////////////
    const dim3 gridSize_nseeds(ceil(d_seeds->size() / static_cast<float>(blockSize.x)));
    kernel_assign_clusters<<<gridSize_nseeds, blockSize.x>>>(d_seeds, d_followers, d_points);
    measureElapsedTime("--- assignClusters:     ", start, stop);

    CUDA_CALL(cudaEventDestroy(start));
    CUDA_CALL(cudaEventDestroy(stop));

    copy_tohost();
}
