#ifndef LayerTilesGPU_h
#define LayerTilesGPU_h

#include <memory>
#include <cmath>
#include <algorithm>
#include <cstdint>

#include <cuda_runtime.h>

#include "GPUVecArray.cuh"
#include "LayerTilesConstants.h"

#include <rmm/device_vector.hpp>

namespace GPU {

    class Matrix : public Managed {
    public:
        Matrix(const rmm::device_uvector<int>& device_data, int rows, int cols) : _rowCount(rows) {
            CUDA_CALL(cudaMallocManaged(&_rows, _rowCount * sizeof(GPU::Vector<int>*)));

            for (int i = 0; i < _rowCount; i++) {
                _rows[i] = new Vector<int>(device_data.element_ptr(i * cols), cols);
            }
        };

        ~Matrix() {
            for (int i = 0; i < _rowCount; i++) {
                delete _rows[i];
            }
            CUDA_CALL(cudaFree(_rows));
        }

        __host__ __device__
        Vector<int>* get(int idx) {
            return _rows[idx];
        }

        __host__ __device__
        const Vector<int>* get(int idx) const {
            return _rows[idx];
        }

        void reset() {
            for (int i = 0; i < _rowCount; i++) {
                _rows[i]->reset();
            }
        }

    protected:
        Matrix(const int* device_data, int rows, int cols) : _rowCount(rows) {
            CUDA_CALL(cudaMallocManaged(&_rows, _rowCount * sizeof(GPU::Vector<int>*)));

            for (int i = 0; i < _rowCount; i++) {
                _rows[i] = new GPU::Vector<int>(device_data + i * cols, cols);
            }
        };

        size_t _rowCount;
        Vector<int>** _rows;
    };


    class LayerTiles : public Matrix {
    public:
        LayerTiles(const int* device_data, int size, int depth) : Matrix(device_data, size, depth) {}

        __device__
        void fill(float x, float y, int i) {
            _rows[getGlobalBin(x, y)]->push_back(i);
        }

        __host__ __device__
        int getXBin(float x) const {
            int xBin = (x - LayerTilesConstants::minX) * LayerTilesConstants::rX;
            return (xBin < LayerTilesConstants::nColumns ? (xBin > 0 ? xBin : 0) : LayerTilesConstants::nColumns - 1);
        }

        __host__ __device__
        int getYBin(float y) const {
            int yBin = (y - LayerTilesConstants::minY) * LayerTilesConstants::rY;
            return (yBin < LayerTilesConstants::nRows ? (yBin > 0 ? yBin : 0) : LayerTilesConstants::nRows - 1);
        }

        __host__ __device__
        int getGlobalBin(float x, float y) const {
            return getXBin(x) + getYBin(y) * LayerTilesConstants::nColumns;
        }

        __host__ __device__
        int getGlobalBinByBin(int xBin, int yBin) const {
            return xBin + yBin * LayerTilesConstants::nColumns;
        }

        __host__ __device__
        int4 searchBox(float xMin, float xMax, float yMin, float yMax) const {
            return int4{getXBin(xMin), getXBin(xMax), getYBin(yMin), getYBin(yMax)};
        }
    };


    class LayerTilesArray : public Managed {
    public:
        LayerTilesArray(const rmm::device_uvector<int>& device_data, int layerCount, int size, int depth)
                : _layerCount(layerCount) {
            CUDA_CALL(cudaMallocManaged(&_layerTilesArray, _layerCount * sizeof(LayerTiles*)));

            for (int i = 0; i < _layerCount; i++) {
                _layerTilesArray[i] = new LayerTiles(device_data.element_ptr(i * size * depth), size, depth);
            }
        }

        ~LayerTilesArray() {
            for (int i = 0; i < _layerCount; i++) {
                delete _layerTilesArray[i];
            }
            CUDA_CALL(cudaFree(_layerTilesArray));
        }

        __device__
        LayerTiles* get(int idx) {
            return _layerTilesArray[idx];
        }

        __device__
        const LayerTiles* get(int idx) const {
            return _layerTilesArray[idx];
        }

        void reset() {
            for (int i = 0; i < _layerCount; i++) {
                _layerTilesArray[i]->reset();
            }
        }

    private:
        size_t _layerCount;
        LayerTiles** _layerTilesArray;
    };
}

#endif
