#pragma once

#include <iostream>
#include <cuda_runtime.h>

namespace GPU {

#define CUDA_CALL(call)     \
{                           \
cudaError_t result = call;  \
if (cudaSuccess != result)  \
    std::cerr << "CUDA error " << result << " in " << __FILE__ << ":" << __LINE__ << ": " << cudaGetErrorString(result) << " (" << call << ")" << std::endl; \
}

class Managed {
public:
    void* operator new(size_t length) {
        void* ptr;
        CUDA_CALL(cudaMallocManaged(&ptr, length));
        return ptr;
    }

    void operator delete(void* ptr) {
        CUDA_CALL(cudaFree(ptr));
    }
};

}
