#ifndef CLUEAlgoGPU_h
#define CLUEAlgoGPU_h

#include "CLUEAlgo.h"
#include "LayerTilesGPU.cuh"

#include <rmm/mr/device/cuda_memory_resource.hpp>
#include <rmm/device_uvector.hpp>

static const int MAX_N_FOLLOWERS = 32;
static const int LOCAL_STACK_SIZE_PER_SEED = MAX_N_FOLLOWERS;

struct PointsPtr {
  float *x; 
  float *y ;
  int *layer ;
  float *weight ;

  float *rho ; 
  float *delta; 
  int *nearestHigher;
  int *clusterIndex; 
  int *isSeed;

  __device__ void setClusterIndex(int i, int value);
};

class CLUEAlgoGPU : public CLUEAlgo {
public:
    CLUEAlgoGPU(int pointCount, float dc, float rhoc, float outlierDeltaFactor, bool verbose,
                rmm::cuda_stream_view& stream_view)
        : _seedsData(pointCount, stream_view), _followersData(pointCount * MAX_N_FOLLOWERS, stream_view),
          _layerTilesData(NLAYERS * LayerTilesConstants::gridSize3D, stream_view),
          CLUEAlgo(dc, rhoc, outlierDeltaFactor, verbose) {
      init_device(pointCount, stream_view);
    }

    ~CLUEAlgoGPU(){
      free_device();
    }

    // public methods
    void makeClusters(); // overwrite base class

  private:
    // algorithm internal variables
    PointsPtr d_points;
    GPU::LayerTilesArray* d_hist;

    rmm::device_uvector<int> _seedsData;
    rmm::device_uvector<int> _followersData;
    rmm::device_uvector<int> _layerTilesData;

    GPU::Vector<int>* d_seeds;
    GPU::Matrix* d_followers;

    // private methods
    void init_device(int pointCount, rmm::cuda_stream_view& stream_view);
    void free_device();
    void copy_todevice();
    void clear_set();
    void copy_tohost();
};

#endif
