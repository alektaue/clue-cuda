#ifndef GPUVecArray_h
#define GPUVecArray_h

#include "GPUCommon.h"
#include <rmm/device_uvector.hpp>

//
// Author: Felice Pantaleo, CERN
//

namespace GPU {

    template<class T>
    class Vector : public Managed {
    public:
        Vector(const rmm::device_uvector<T>& device_vector) : m_data(const_cast<T*>(device_vector.data())),
                                                              m_capacity(device_vector.capacity()) {}

        Vector(const T* data, int capacity) : m_data(const_cast<T*>(data)), m_capacity(capacity) {}

        inline constexpr int push_back_unsafe(const T& element) {
            auto previousSize = m_size;
            m_size++;
            if (previousSize < m_capacity) {
                m_data[previousSize] = element;
                return previousSize;
            } else {
                --m_size;
                return -1;
            }
        }

        template<class... Ts>
        constexpr int emplace_back_unsafe(Ts&& ... args) {
            auto previousSize = m_size;
            m_size++;
            if (previousSize < m_capacity) {
                (new(&m_data[previousSize]) T(std::forward<Ts>(args)...));
                return previousSize;
            } else {
                --m_size;
                return -1;
            }
        }

        inline constexpr T& back() const {
            assert(m_size > 0);
            return m_data[m_size - 1];
        }

        __device__
        int push_back(const T& element);

        template<class... Ts>
        __device__
        int emplace_back(Ts&& ... args);

        __host__ __device__
        inline T pop_back() {
            if (m_size > 0) {
                auto previousSize = m_size--;
                return m_data[previousSize - 1];
            } else
                return T();
        }

        __host__ __device__
        T const* begin() const { return m_data; }

        __host__ __device__
        T const* end() const { return m_data + m_size; }

        __host__ __device__
        T* begin() { return m_data; }

        __host__ __device__
        T* end() { return m_data + m_size; }

        __host__ __device__
        int size() const { return m_size; }

        __host__ __device__
        T& operator[](int idx) {
            return m_data[idx];
        }

        __host__ __device__
        const T& operator[](int idx) const {
            return m_data[idx];
        }

        inline constexpr void reset() { m_size = 0; }

        inline constexpr int capacity() const { return m_capacity; }

        inline constexpr T const* data() const { return m_data; }

        inline constexpr void resize(int size) { m_size = size; }

        inline constexpr bool empty() const { return 0 == m_size; }

        inline constexpr bool full() const { return m_capacity == m_size; }

        int m_capacity;
        int m_size = 0;
        T* m_data;
    };

} // end namespace GPU

#endif // GPUVecArray_h
